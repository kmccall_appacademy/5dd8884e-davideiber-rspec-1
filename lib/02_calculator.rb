def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(array)
  if array.length > 1
    total_sum = 0
    array.each do |el|
      total_sum = total_sum + el
    end
    total_sum
  elsif array.length == 1
    array[0]
  else
    0
  end
end

def multiply(a, b)
  a * b
end


def power(a, b)
  a**b
end


def factorial(a)
  if a == 0
    0
  else
    (1..a).to_a.reduce(:*)
  end
end
