def echo(word)
  word
end

def shout(word)
  word.upcase
end

def shout(word)
  word.upcase
end

def repeat(word, x = 1)
  word_array = [word]
  if x == 1
    word_array.push(word)
    return word_array.join(" ")
  else
    (x - 1).times do
      word_array.push(word)
    end
    word_array.join(" ")
  end
end

def start_of_word(word, num)
  word.slice(0, num)
end

def first_word(sentence)
  sentence_array = sentence.split
  sentence_array[0]
end

def titleize(sentence)
  little_words = ["the", "and", "over"]
  sentence_array = sentence.split
  capital_array = []
  sentence_array.each do |word| 
    if little_words.include?(word)
      capital_array << word
    else
      capital_array << word.capitalize
    end
  end
  capital_array[0].capitalize!
  capital_array.join(" ")
end
