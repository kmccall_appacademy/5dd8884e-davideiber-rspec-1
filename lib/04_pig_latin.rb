def pigify(word)
  vowels_array = ["a", "e", "i", "o", "u"]
  if vowels_array.include?(word[0])
    word + "ay"
  elsif word[0..1] == "qu"
    word[2..word.length - 1] + "quay"
  elsif word[1..2] == "qu" && !vowels_array.include?(word[0])
    word[3..word.length - 1] + word[0..1] + "uay"
  else
    idx = 0
    until vowels_array.include?(word[idx])
      idx += 1
    end
    word[idx..word.length - 1] + word[0...idx] + "ay"
  end
end

def translate(phrase)
  translated = []
  phrase.split.each do |el|
    translated.push(pigify(el))
  end
  translated.join(" ")
end
